package hatal.hangoutsapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "location")
public class Location {

    @Id
    @JsonProperty
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty
    @Column(name = "street")
    private String street;

    @JsonProperty
    @Column(name = "number")
    private String number;

    @JsonProperty
    @Column(name = "city")
    private String city;

    @JsonProperty
    @Column(name = "country")
    private String country;
    
    publi
}
    
    
