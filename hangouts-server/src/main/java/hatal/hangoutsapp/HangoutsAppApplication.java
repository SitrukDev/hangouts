package hatal.hangoutsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HangoutsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(HangoutsAppApplication.class, args);
	}

}
